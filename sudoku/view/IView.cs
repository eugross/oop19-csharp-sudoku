﻿using Sudoku.utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku.view
{
    public interface IView
    {
        void BoardUpdate(List<KeyValuePair<int, bool>> values);

        void SetScene(Scenes scene);
    }
}
