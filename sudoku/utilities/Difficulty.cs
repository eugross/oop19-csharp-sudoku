﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku.utilities
{
    public enum Difficulty
    {
        EASY,
        MEDIUM,
        HARD
    }
}
