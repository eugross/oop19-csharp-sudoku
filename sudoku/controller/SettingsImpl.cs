﻿using Sudoku.utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku.controller
{
    class SettingsImpl : ISettings
    {
        private Theme theme;
        private bool darkTheme;
        private bool assists;

        public SettingsImpl()
        {
            theme = Theme.LIGHT;
        }

        public bool DarkTheme 
        { 
            get => darkTheme;

            set
            {
                darkTheme = !darkTheme;
                theme = darkTheme ? Theme.DARK : Theme.LIGHT;
            }
        }
        public bool Assists 
        { 
            get => assists;

            set => assists = !assists;
        }

        public Theme GetTheme()
        {
            return theme;
        }
    }
}
