﻿using Sudoku.utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku.controller
{
    public interface ISettings
    {
        public bool DarkTheme { get; set; }

        public bool Assists { get; set; }

        public Theme GetTheme();
    }
}
