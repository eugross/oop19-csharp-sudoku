﻿using Sudoku.controller;
using Sudoku.utilities;
using Sudoku.view;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sudoku.controller
{
    public class SudokuGameHandlerImpl : ISudokuGameHandler
    {

        private static readonly int SUDOKU_SIZE = 9;
        private static readonly int SUDOKU_SQUARE_SIZE = 3;
        private static readonly int INITIAL_LINE = 0;
        private static readonly int SOLUTION_LINE = 1;
        private static readonly string SUDOKU_SEPARATOR = ",";
        private static readonly string FORMAT = ".txt";
        private static readonly string SAVE_FORMAT = ".obj";
        private static readonly string SEPARATOR = Path.DirectorySeparatorChar.ToString();
        private static readonly string DIRECTORY_PATH = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + SEPARATOR + ".SuDoKu_C#";
        private static readonly string SAVE_PATH = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + SEPARATOR + ".SuDoKu_C#" + SEPARATOR + "GameData" ;
        private readonly Random rand = new Random();
        private readonly ISettings settings;
        private readonly IView view;
        private ISudokuLogic model;

        public SudokuGameHandlerImpl(IView view)
        {
            settings = new SettingsImpl();
            this.view = view;
        }

        public ISudokuLogic GetLogic()
        {
            return model;
        }

        public ISettings GetSettings()
        {
            return settings;
        }

        public void HitCell(int x, int y, int value)
        {
            model.Hit(x, y, value);
            view.BoardUpdate(model.GetValues());
        }

        public void RemoveCell(int x, int y)
        {
            model.Remove(x, y);
            view.BoardUpdate(model.GetValues());
        }

        public void NewGame(Difficulty diff)
        {
            string[] selected = SelectSudoku(LoadFromFile(SelectPath(diff) + FORMAT));
            int[] i1 = selected[INITIAL_LINE].ToCharArray()
                                             .Select(c => int.Parse(c.ToString()))
                                             .ToArray();
            int[] i2 = selected[SOLUTION_LINE].ToCharArray()
                                              .Select(c => int.Parse(c.ToString()))
                                              .ToArray();
            model = new SudokuLogicImpl(i1, i2, SUDOKU_SIZE, SUDOKU_SQUARE_SIZE);
            view.SetScene(Scenes.GAME);
        }

        public bool PreviousGameExist()
        {
            return !File.Exists(SAVE_PATH);
        }     

        public void ResumeGame()
        {
            if (Directory.Exists(SAVE_PATH))
            {
                FileStream fileStream = new FileStream(SAVE_PATH+SAVE_FORMAT, FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                ISudokuLogic board = (ISudokuLogic) bf.Deserialize(fileStream);
                model = board;
                fileStream.Close();
            }
        }

        public void SaveGame()
        {            
            SaveFile(SAVE_PATH, model);
            view.SetScene(Scenes.HOME);
            
        }

        private void SaveFile(string path, object obj)
        {
            if (!Directory.Exists(path) || File.Exists(SAVE_PATH))
            {
                Directory.CreateDirectory(DIRECTORY_PATH);
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fileStream = new FileStream(path+SAVE_FORMAT, FileMode.Create);
                try
                {
                    bf.Serialize(fileStream, obj);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    fileStream.Close();
                }
            }
        }
        private List<string> LoadFromFile(string path)
        {
            return System.IO.File.ReadLines(path).ToList();
        }
        private string SelectPath(Difficulty diff)
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"sudokugrid\" + diff.ToString());
        }

        private string[] SelectSudoku(List<string> list)
        {
            return list[rand.Next(list.Count - 1)].Split(SUDOKU_SEPARATOR);
        }
    }
}
