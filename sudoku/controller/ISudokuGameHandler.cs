﻿using Sudoku.utilities;

namespace Sudoku.controller
{
    public interface ISudokuGameHandler
    {
        void NewGame(Difficulty diff);

        void ResumeGame();

        void SaveGame();

        bool PreviousGameExist();

        void HitCell(int x, int y, int value);

        void RemoveCell(int x, int y);

        ISettings GetSettings();

        ISudokuLogic GetLogic();
    }
}
