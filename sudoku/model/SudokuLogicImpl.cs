﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sudoku.controller
{
    [Serializable]
    public class SudokuLogicImpl : ISudokuLogic
    {
        private static readonly int EMPTY_VALUE = 0;
        public int Size { get; }
        public int SquareSize { get; }
        public int[,] Solution { get; }
        private readonly SortedDictionary<KeyValuePair<int, int>, int> InitialGrid;
        private readonly SortedDictionary<KeyValuePair<int, int>, KeyValuePair<int, bool>> SudokuGrid;


        public SudokuLogicImpl(int[] initialGrid, int[] solution, int size, int squareSize)
        {
            InitialGrid = new SortedDictionary<KeyValuePair<int, int>, int>(new PairComparer());
            SudokuGrid = new SortedDictionary<KeyValuePair<int, int>, KeyValuePair<int, bool>>(new PairComparer());
            this.Size = size;
            this.SquareSize = squareSize;
            this.Solution = new int[this.Size, this.Size];
            int c = 0;
            for (int i = 0; i < this.Size; i++)
            {
                for (int j = 0; j < this.Size; j++)
                {
                    InitialGrid.Add(new KeyValuePair<int, int>(i, j), initialGrid[c]);
                    SudokuGrid.Add(new KeyValuePair<int, int>(i, j), new KeyValuePair<int, bool>(initialGrid[c], initialGrid[c] != EMPTY_VALUE));
                    this.Solution[i, j] = solution[c];
                    c++;
                }
            }
        }

        public void Hit(int x, int y, int value)
        {
            KeyValuePair<int, int> p = new KeyValuePair<int, int>(x, y);
            if (InitialGrid[p].Equals(EMPTY_VALUE))
            {
                SudokuGrid[p] = new KeyValuePair<int, bool>(value, CheckRow(x, value) && CheckColumn(y, value) && CheckSquare(x, y, value));
            }
        }

        public void Remove(int x, int y)
        {
            KeyValuePair<int, int> p = new KeyValuePair<int, int>(x, y);
            if (InitialGrid[p].Equals(EMPTY_VALUE))
            {
                SudokuGrid[p] = new KeyValuePair<int, bool>(EMPTY_VALUE, false);
            }
        }

        public bool IsDone()
        {
            return SudokuGrid.All(i => i.Value.Value.Equals(true));
        }

        public string[] GetInitialGrid()
        {
            return InitialGrid.Select(i => i.Value.ToString()).ToArray();
        }
        public List<KeyValuePair<int, bool>> GetValues()
        {
            return SudokuGrid.Values.ToList();
        }
        private bool CheckRow(int x, int value)
        {
            return !SudokuGrid.Where(r => r.Key.Key.Equals(x))
                             .Any(m => m.Value.Key.Equals(value));  
        }
        private bool CheckColumn(int y, int value)
        {
            return !SudokuGrid.Where(c => c.Key.Value.Equals(y))
                             .Any(m => m.Value.Key.Equals(value));
        }
        private bool CheckSquare(int x, int y, int value)
        {
            int subX = x - x % SquareSize;
            int subY = y - y % SquareSize;
            return !SudokuGrid.Where(r => r.Key.Key >= subX && r.Key.Key < (subX + SquareSize))
                             .Where(c => c.Key.Value >= subY && c.Key.Value < (subY + SquareSize))
                             .Any(m => m.Value.Key.Equals(value));
        }

        [Serializable]
        private class PairComparer : IComparer<KeyValuePair<int,int>>
        {
            int IComparer<KeyValuePair<int, int>>.Compare(KeyValuePair<int, int> p1, KeyValuePair<int, int> p2)
            {
                return p1.Key.Equals(p2.Key) ? p1.Value.CompareTo(p2.Value) : p1.Key.CompareTo(p2.Key);
            }
        }
    }
}
