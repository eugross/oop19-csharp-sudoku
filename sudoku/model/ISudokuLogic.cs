﻿using System.Collections.Generic;

namespace Sudoku.controller
{
    public interface ISudokuLogic
    {
        public void Hit(int x, int y, int value);
        public void Remove(int x, int y);
        public bool IsDone();
        public int Size { get; }
        public int SquareSize { get; }
        public int[,] Solution { get; }
        public List<KeyValuePair<int, bool>> GetValues();
        public string[] GetInitialGrid();

    }
}
