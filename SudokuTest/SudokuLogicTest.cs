﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Sudoku.controller;
using System.Linq;

namespace SudokuTest
{
    [TestClass]
    public class SudokuLogicTest
    {
        private static readonly int SUDOKU_SIZE = 9;
        private static readonly int SUDOKU_SQUARE_SIZE = 3;
        private static int[] grid;
        private static List<KeyValuePair<int, bool>> values;
        private static readonly int[] initialGrid =  {0, 0, 7, 4, 0, 0, 0, 0, 0,
                                                      0, 0, 0, 0, 0, 0, 0, 7, 0,
                                                      0, 0, 0, 0, 1, 0, 0, 0, 0,
                                                      8, 3, 0, 0, 0, 0, 0, 0, 0,
                                                      0, 1, 4, 0, 0, 0, 0, 2, 8,
                                                      0, 0, 0, 2, 0, 0, 3, 6, 0,
                                                      5, 0, 0, 0, 0, 3, 7, 0, 0,
                                                      3, 0, 0, 0, 0, 4, 5, 1, 0,
                                                      4, 8, 0, 0, 5, 6, 0, 3, 0};

        private static readonly int[] solution = {1, 5, 7, 4, 8, 2, 6, 9, 3,
                                                  2, 4, 8, 6, 3, 9, 1, 7, 5,
                                                  9, 6, 3, 5, 1, 7, 8, 4, 2,
                                                  8, 3, 2, 9, 6, 1, 4, 5, 7,
                                                  6, 1, 4, 3, 7, 5, 9, 2, 8,
                                                  7, 9, 5, 2, 4, 8, 3, 6, 1,
                                                  5, 2, 6, 1, 9, 3, 7, 8, 4,
                                                  3, 7, 9, 8, 2, 4, 5, 1, 6,
                                                  4, 8, 1, 7, 5, 6, 2, 3, 9};

        private readonly ISudokuLogic logic = new SudokuLogicImpl(initialGrid, solution, SUDOKU_SIZE, SUDOKU_SQUARE_SIZE);

        [TestMethod]
        public void TestInitial()
        {
            grid = logic.GetInitialGrid().Select(x => int.Parse(x)).ToArray();
            values = logic.GetValues();
            int c = 0;
            for(int i = 0; i < SUDOKU_SIZE; i++) 
            {
                for (int j = 0; j < SUDOKU_SIZE; j++)
                {            Assert.AreEqual(SUDOKU_SIZE, logic.Size);

                    Assert.AreEqual(initialGrid[c], grid[c]);
                    Assert.AreEqual(values[c], new KeyValuePair<int, bool>(initialGrid[c], !initialGrid[c].Equals(0)));
                    Assert.AreEqual(solution[c], logic.Solution[i,j]);
                    c++;
                }
            }
            Assert.AreEqual(SUDOKU_SQUARE_SIZE, logic.SquareSize);
        }

        [TestMethod]
        public void TestHit()
        {
            logic.Hit(0, 0, 5);
            logic.Hit(0, 2, 5);
            logic.Hit(1, 3, 9);
            values = logic.GetValues();

            Assert.AreEqual(values[0], new KeyValuePair<int, bool>(5, false));
            Assert.AreEqual(values[2], new KeyValuePair<int, bool>(7, true));
            Assert.AreEqual(values[12], new KeyValuePair<int, bool>(9, true));
        }

        [TestMethod]
        public void TestRemove()
        {
            logic.Remove(0, 0);
            logic.Remove(0, 2);
            logic.Remove(1, 3);
            values = logic.GetValues();

            Assert.AreEqual(values[0], new KeyValuePair<int, bool>(0, false));
            Assert.AreEqual(values[2], new KeyValuePair<int, bool>(7, true));
            Assert.AreEqual(values[12], new KeyValuePair<int, bool>(0, false));
        }

        [TestMethod]
        public void TestisDone()
        {
            Assert.AreEqual(logic.IsDone(), false);
            int c = 0;
            for (int i = 0; i < SUDOKU_SIZE; i++)
            {
                for (int j = 0; j < SUDOKU_SIZE; j++)
                {
                    logic.Hit(i, j, solution[c]);
                    c++;
                }
            }
            Assert.AreEqual(logic.IsDone(), true);

        }
    }
}
