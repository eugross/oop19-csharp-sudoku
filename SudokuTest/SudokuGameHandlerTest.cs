﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sudoku.controller;
using Sudoku.utilities;
using Sudoku.view;
using System.Collections.Generic;

namespace SudokuTest
{
    [TestClass]
    public class SudokuGameHandlerTest
    {
        private static readonly int SUDOKU_SIZE = 9;
        private readonly ISudokuGameHandler gh = new SudokuGameHandlerImpl(new ViewImpl());
        private List<KeyValuePair<int, bool>> list;
        private List<KeyValuePair<int, bool>> values;
        private ISudokuLogic logic;

        [TestMethod]
        public void TestResume()
        {
            gh.NewGame(Difficulty.EASY);
            gh.HitCell(0, 0, 9);
            gh.HitCell(1, 3, 5);
            gh.RemoveCell(1, 5);
            logic = gh.GetLogic();
            list = logic.GetValues();
            gh.SaveGame();
            gh.ResumeGame();
            values = gh.GetLogic().GetValues();
            for (int i = 0; i < SUDOKU_SIZE*SUDOKU_SIZE; i++)
            {
                Assert.AreEqual(list[i].Key, values[i].Key);
            }
        }

        [TestMethod]
        public void TestEndGame()
        {
            gh.NewGame(Difficulty.MEDIUM);
            logic = gh.GetLogic();
            list = logic.GetValues();
            gh.SaveGame();
            gh.ResumeGame();
            int[,] sol = gh.GetLogic().Solution;
            Assert.AreEqual(gh.GetLogic().IsDone(), false);
            int c = 0;
            for (int i = 0; i < SUDOKU_SIZE; i++)
            {
                for (int j = 0; j < SUDOKU_SIZE; j++)
                {
                    gh.GetLogic().Hit(i, j, sol[i, j]);
                    c++;
                }
            }
            Assert.AreEqual(gh.GetLogic().IsDone(), true);
        }

    }
}
